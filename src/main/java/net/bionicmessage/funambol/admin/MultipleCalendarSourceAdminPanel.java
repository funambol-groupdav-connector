/*
 * MultipleCalendarSourceAdminPanel.java
 *
 * Created on September 21, 2007, 9:38 PM, by Mathew McBride
 * 
 * GroupDAV connector for Funambol v6.5
 * Copyright (C) 2007  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.admin;

import com.funambol.admin.ui.SourceManagementPanel;
import com.funambol.framework.engine.source.ContentType;
import com.funambol.framework.engine.source.SyncSourceInfo;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import net.bionicmessage.funambol.BVersion;
import net.bionicmessage.funambol.framework.Constants;
import net.bionicmessage.funambol.groupdav.calendar.CalendarSyncSource;
import net.bionicmessage.objects.StoreConstants;

/**
 *
 * @author  matt
 */
public class MultipleCalendarSourceAdminPanel extends SourceManagementPanel {

    private CalendarSyncSource calSource;
    private boolean taskMode = false;
    /** Creates new form MultipleCalendarSourceAdminPanel */

    public MultipleCalendarSourceAdminPanel() {
        initComponents();
    }

    public MultipleCalendarSourceAdminPanel(boolean taskMode) {
        this.taskMode = taskMode;
    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        srvTypeButtonGroup = new javax.swing.ButtonGroup();
        scrollPane = new javax.swing.JScrollPane();
        mainPanel = new javax.swing.JPanel();
        formTitle = new javax.swing.JLabel();
        sourceId_label = new javax.swing.JLabel();
        sourceId = new javax.swing.JTextField();
        serverHost = new javax.swing.JTextField();
        cdataType_label = new javax.swing.JLabel();
        cdataType = new javax.swing.JTextField();
        vcalNote = new javax.swing.JLabel();
        sifeNote = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        sourcesTable = new javax.swing.JTable();
        options_label = new javax.swing.JLabel();
        singleLog = new javax.swing.JCheckBox();
        emailSubstitute = new javax.swing.JCheckBox();
        zideStore = new javax.swing.JCheckBox();
        saveButton = new javax.swing.JButton();
        default_note = new javax.swing.JLabel();
        storeLoc_label = new javax.swing.JLabel();
        storeLoc = new javax.swing.JTextField();
        inboundClass = new javax.swing.JTextField();
        inboundClass_label = new javax.swing.JLabel();
        warning = new javax.swing.JLabel();
        sourcesTable_label = new javax.swing.JLabel();
        serverHost_label = new javax.swing.JLabel();
        dualtodo = new javax.swing.JCheckBox();
        convertAlarms = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        cDAVComponentTypeLabel = new javax.swing.JLabel();
        cdav_component = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        cdav_property = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cdav_start = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        cdav_end = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        gdavType = new javax.swing.JRadioButton();
        calDavType = new javax.swing.JRadioButton();
        autodetectType = new javax.swing.JRadioButton();
        dlLabel = new javax.swing.JLabel();
        cDLBulk = new javax.swing.JTextField();
        atOnceLabel = new javax.swing.JLabel();
        cDAVConcurrent = new javax.swing.JTextField();
        concurrentLabel = new javax.swing.JLabel();

        setLayout(new java.awt.BorderLayout());

        scrollPane.setAutoscrolls(true);
        scrollPane.setMaximumSize(new java.awt.Dimension(900, 900));
        scrollPane.setPreferredSize(new java.awt.Dimension(300, 300));

        mainPanel.setMaximumSize(new java.awt.Dimension(900, 1200));
        mainPanel.setPreferredSize(new java.awt.Dimension(300, 700));
        mainPanel.setLayout(null);

        formTitle.setFont(new java.awt.Font("Lucida Grande", 0, 24));
        formTitle.setText("GroupDAV Calendar Sync Source"); // NOI18N
        mainPanel.add(formTitle);
        formTitle.setBounds(10, 0, 381, 30);

        sourceId_label.setText("SyncML ID"); // NOI18N
        mainPanel.add(sourceId_label);
        sourceId_label.setBounds(10, 40, 65, 16);

        sourceId.setText("groupdav-cal"); // NOI18N
        mainPanel.add(sourceId);
        sourceId.setBounds(130, 40, 310, 28);

        serverHost.setText("http://host:port/"); // NOI18N
        mainPanel.add(serverHost);
        serverHost.setBounds(130, 70, 310, 28);

        cdataType_label.setText("Client data type"); // NOI18N
        mainPanel.add(cdataType_label);
        cdataType_label.setBounds(10, 100, 99, 16);

        cdataType.setText("text/x-vcalendar"); // NOI18N
        mainPanel.add(cdataType);
        cdataType.setBounds(130, 100, 310, 28);

        vcalNote.setFont(new java.awt.Font("Lucida Grande", 0, 11));
        vcalNote.setText("text/x-vcalendar for most clients"); // NOI18N
        mainPanel.add(vcalNote);
        vcalNote.setBounds(130, 120, 179, 30);

        sifeNote.setFont(new java.awt.Font("Lucida Grande", 0, 11));
        sifeNote.setText("text/x-s4j-sife for Funambol clients. text/x-s4j-sift for Funambol tasks"); // NOI18N
        mainPanel.add(sifeNote);
        sifeNote.setBounds(130, 140, 400, 14);

        sourcesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"default", "/groupdav/Calendar/"},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Source Name", "Server Path"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(sourcesTable);

        mainPanel.add(jScrollPane1);
        jScrollPane1.setBounds(130, 190, 454, 140);

        options_label.setText("Options"); // NOI18N
        mainPanel.add(options_label);
        options_label.setBounds(10, 440, 50, 16);

        singleLog.setText("Use single log files (RECOMMENDED for production use)"); // NOI18N
        singleLog.setMargin(new java.awt.Insets(0, 0, 0, 0));
        mainPanel.add(singleLog);
        singleLog.setBounds(130, 440, 383, 22);

        emailSubstitute.setText("Substitute username for email address on GroupDAV server"); // NOI18N
        emailSubstitute.setMargin(new java.awt.Insets(0, 0, 0, 0));
        mainPanel.add(emailSubstitute);
        emailSubstitute.setBounds(130, 480, 403, 20);

        zideStore.setText("OGo / multi timezone all day event handling"); // NOI18N
        zideStore.setMargin(new java.awt.Insets(0, 0, 0, 0));
        mainPanel.add(zideStore);
        zideStore.setBounds(130, 500, 390, 22);

        saveButton.setText("Save configuration"); // NOI18N
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        mainPanel.add(saveButton);
        saveButton.setBounds(430, 680, 161, 30);

        default_note.setText("NOTE: there MUST be a 'default' source. Use %USER% for OGo multiuser "); // NOI18N
        mainPanel.add(default_note);
        default_note.setBounds(130, 340, 460, 16);

        storeLoc_label.setText("Store location"); // NOI18N
        mainPanel.add(storeLoc_label);
        storeLoc_label.setBounds(10, 160, 110, 16);

        storeLoc.setText("/opt/Funambol/stores/groupdav-cal"); // NOI18N
        mainPanel.add(storeLoc);
        storeLoc.setBounds(130, 160, 310, 28);
        mainPanel.add(inboundClass);
        inboundClass.setBounds(130, 390, 450, 28);

        inboundClass_label.setText("Inbound processor"); // NOI18N
        mainPanel.add(inboundClass_label);
        inboundClass_label.setBounds(10, 360, 120, 20);

        warning.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        warning.setText("(Advanced setting. Leave blank unless you use one)"); // NOI18N
        mainPanel.add(warning);
        warning.setBounds(130, 420, 390, 13);

        sourcesTable_label.setText("Calendar sources"); // NOI18N
        mainPanel.add(sourcesTable_label);
        sourcesTable_label.setBounds(10, 190, 108, 16);

        serverHost_label.setText("GroupDAV Server"); // NOI18N
        mainPanel.add(serverHost_label);
        serverHost_label.setBounds(10, 70, 107, 16);

        dualtodo.setText("Dual event/todo sync source (for Symbian S60)");
        dualtodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dualtodoActionPerformed(evt);
            }
        });
        mainPanel.add(dualtodo);
        dualtodo.setBounds(130, 360, 350, 23);

        convertAlarms.setText("Convert alarms (use with EXTREME caution)");
        mainPanel.add(convertAlarms);
        convertAlarms.setBounds(130, 460, 310, 20);

        jLabel1.setText("CalDAV filtering");
        mainPanel.add(jLabel1);
        jLabel1.setBounds(20, 620, 110, 16);

        cDAVComponentTypeLabel.setText("Component");
        mainPanel.add(cDAVComponentTypeLabel);
        cDAVComponentTypeLabel.setBounds(150, 620, 90, 16);

        cdav_component.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cdav_componentActionPerformed(evt);
            }
        });
        mainPanel.add(cdav_component);
        cdav_component.setBounds(240, 620, 120, 28);

        jLabel3.setText("Property");
        mainPanel.add(jLabel3);
        jLabel3.setBounds(370, 620, 80, 16);
        mainPanel.add(cdav_property);
        cdav_property.setBounds(440, 620, 140, 28);

        jLabel4.setText("Start");
        mainPanel.add(jLabel4);
        jLabel4.setBounds(150, 660, 29, 16);
        mainPanel.add(cdav_start);
        cdav_start.setBounds(240, 650, 120, 28);

        jLabel5.setText("End");
        mainPanel.add(jLabel5);
        jLabel5.setBounds(370, 650, 23, 16);
        mainPanel.add(cdav_end);
        cdav_end.setBounds(440, 650, 140, 28);

        jLabel6.setText("Server Type");
        mainPanel.add(jLabel6);
        jLabel6.setBounds(10, 530, 80, 16);

        srvTypeButtonGroup.add(gdavType);
        gdavType.setText("GroupDAV");
        gdavType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gdavTypeActionPerformed(evt);
            }
        });
        mainPanel.add(gdavType);
        gdavType.setBounds(280, 530, 97, 23);

        srvTypeButtonGroup.add(calDavType);
        calDavType.setText("CalDAV/GroupDAV 2");
        mainPanel.add(calDavType);
        calDavType.setBounds(380, 530, 170, 23);

        srvTypeButtonGroup.add(autodetectType);
        autodetectType.setSelected(true);
        autodetectType.setText("Autodetect (default)");
        mainPanel.add(autodetectType);
        autodetectType.setBounds(120, 530, 160, 23);

        dlLabel.setText("Download");
        mainPanel.add(dlLabel);
        dlLabel.setBounds(120, 560, 80, 16);

        cDLBulk.setText("500");
        cDLBulk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cDLBulkActionPerformed(evt);
            }
        });
        mainPanel.add(cDLBulk);
        cDLBulk.setBounds(190, 580, 40, 28);

        atOnceLabel.setText("at once (cDAV/gDAV2)");
        mainPanel.add(atOnceLabel);
        atOnceLabel.setBounds(230, 585, 180, 16);

        cDAVConcurrent.setText("2");
        mainPanel.add(cDAVConcurrent);
        cDAVConcurrent.setBounds(190, 555, 20, 28);

        concurrentLabel.setText("concurrently (GroupDAV)");
        mainPanel.add(concurrentLabel);
        concurrentLabel.setBounds(220, 560, 160, 16);

        scrollPane.setViewportView(mainPanel);

        add(scrollPane, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings("deprecation")
private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        ContentType ctype = new ContentType();
        ctype.setType(cdataType.getText());
        ctype.setVersion("1.0");
        ContentType[] holder = new ContentType[1];
        holder[0] = ctype;
        SyncSourceInfo ssInfo = new SyncSourceInfo(holder, 0);
        calSource.setInfo(ssInfo);
        calSource.setType(cdataType.getText());
        calSource.setSourceURI(sourceId.getText());
        calSource.setName(sourceId.getText());
        Properties sourceSettings = new Properties();
        sourceSettings.setProperty(Constants.SERVER_HOST, serverHost.getText());
        int tableRowCount = sourcesTable.getModel().getRowCount();
        for (int curRow = 0; curRow < tableRowCount; curRow++) {
            String ssName = (String) sourcesTable.getModel().getValueAt(curRow, 0);
            if (ssName != null && ssName.length() > 0) {
                String ssValue = (String) sourcesTable.getModel().getValueAt(curRow, 1);
                sourceSettings.setProperty(Constants.SOURCE_LOCATION_BASE + ssName, ssValue);
            }
        }
        if (singleLog.isSelected()) {
            sourceSettings.setProperty(Constants.SINGLE_LOG, "");
        }
        sourceSettings.setProperty(Constants.STOREDIR_PATH, storeLoc.getText());

        if (inboundClass.getText().length() > 0) {
            sourceSettings.setProperty(Constants.INBOUND_PROCESSOR, inboundClass.getText());
        }
        if (dualtodo.isSelected()) {
            sourceSettings.setProperty(Constants.MIXED_MODE, "");
        }
        if (zideStore.isSelected()) {
            sourceSettings.setProperty(Constants.UTC_RELATIVE_ALLDAY, "true");
        }
        if (convertAlarms.isSelected()) {
            sourceSettings.setProperty(Constants.CONVERT_ALARMS,"true");
        }
        if (cdav_component.getText().length() > 0) {
            sourceSettings.setProperty(Constants.CALDAV_COMPONENT,
                    cdav_component.getText());
            sourceSettings.setProperty(Constants.CALDAV_PROPERTY,
                    cdav_property.getText());
            sourceSettings.setProperty(Constants.CALDAV_START,
                    cdav_start.getText());
            sourceSettings.setProperty(Constants.CALDAV_END, 
                    cdav_end.getText());
        }
        if (gdavType.isSelected()) {
            sourceSettings.setProperty(StoreConstants.PROPERTY_SERVER_MODE,"groupdav");
        } else if (calDavType.isSelected()) {
            sourceSettings.setProperty(StoreConstants.PROPERTY_SERVER_MODE,"caldav");
        } 
        sourceSettings.setProperty(StoreConstants.PROPERTY_SERVER_CTHREADS, cDAVConcurrent.getText());
        sourceSettings.setProperty(StoreConstants.PROPERTY_SERVER_ITEMS, cDLBulk.getText());
        calSource.setConnectorProperties(sourceSettings);
        if (getState() == STATE_INSERT) {
            this.actionPerformed(new ActionEvent(this, ACTION_EVENT_INSERT, evt.getActionCommand()));
        } else {
            this.actionPerformed(new ActionEvent(this, ACTION_EVENT_UPDATE, evt.getActionCommand()));
        }
    
}//GEN-LAST:event_saveButtonActionPerformed

    private void dualtodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dualtodoActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_dualtodoActionPerformed

private void cdav_componentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cdav_componentActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_cdav_componentActionPerformed

private void gdavTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gdavTypeActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_gdavTypeActionPerformed

private void cDLBulkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cDLBulkActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_cDLBulkActionPerformed

    public void updateForm() {
        calSource = (CalendarSyncSource) getSyncSource();
        SyncSourceInfo ssInfo = calSource.getInfo();
        if (ssInfo != null) {
            cdataType.setText(ssInfo.getPreferredType().getType());
        }
        sourceId.setText(calSource.getSourceURI());
        if (calSource.getConnectorProperties() != null) {
            Properties sourceSettings = calSource.getConnectorProperties();
            serverHost.setText(sourceSettings.getProperty(Constants.SERVER_HOST));
            DefaultTableModel mod = new DefaultTableModel();
            mod.setRowCount(sourceSettings.keySet().size());
            sourcesTable.setModel(mod);
            mod.setColumnCount(2);
            storeLoc.setText(sourceSettings.getProperty(Constants.STOREDIR_PATH));
            Vector identifiers = new Vector();
            identifiers.add("Source Name");
            identifiers.add("Server Path");
            mod.setColumnIdentifiers(identifiers);
            Iterator keyIt = sourceSettings.keySet().iterator();
            int curRow = 0;
            while (keyIt.hasNext()) {
                String keyName = (String) keyIt.next();
                if (keyName.contains(Constants.SOURCE_LOCATION_BASE)) {
                    String ssName = keyName.replace(Constants.SOURCE_LOCATION_BASE, "");
                    String ssLoc = sourceSettings.getProperty(keyName);
                    sourcesTable.getModel().setValueAt(ssName, curRow, 0);
                    sourcesTable.getModel().setValueAt(ssLoc, curRow, 1);
                    curRow++;
                }
            }
            if (sourceSettings.getProperty(Constants.SINGLE_LOG) != null) {
                singleLog.setSelected(true);
            }
            if (sourceSettings.getProperty(Constants.INBOUND_PROCESSOR) != null) {
                inboundClass.setText(inboundClass.getText());
            }
            if (sourceSettings.getProperty(Constants.MIXED_MODE) != null) {
                dualtodo.setSelected(true);
            }
            if (sourceSettings.getProperty(Constants.UTC_RELATIVE_ALLDAY) != null) {
                zideStore.setSelected(true);
            }
            if (sourceSettings.getProperty(Constants.CONVERT_ALARMS) != null) {
                convertAlarms.setSelected(true);
            }
            String calDavComp = sourceSettings.getProperty(Constants.CALDAV_COMPONENT);
            String calDavProp = sourceSettings.getProperty(Constants.CALDAV_PROPERTY);
            String calDavStart = sourceSettings.getProperty(Constants.CALDAV_START);
            String calDavEnd = sourceSettings.getProperty(Constants.CALDAV_END);
            if (calDavComp != null) {
                cdav_component.setText(calDavComp);
                cdav_property.setText(calDavProp);
                cdav_start.setText(calDavStart);
                cdav_end.setText(calDavEnd);
            }
            String srvType = sourceSettings.getProperty(StoreConstants.PROPERTY_SERVER_MODE);
            if (srvType != null) {
                if (srvType.equals("groupdav")) {
                    gdavType.setSelected(true);
                } else if (srvType.equals("caldav")) {
                    gdavType.setSelected(true);
                }
            }
            String cthreads = sourceSettings.getProperty(StoreConstants.PROPERTY_SERVER_CTHREADS);
            if (cthreads != null) {
                cDAVConcurrent.setText(cthreads);
            } 
            String citems = sourceSettings.getProperty(StoreConstants.PROPERTY_SERVER_ITEMS);
            if (citems != null) {
                cDLBulk.setText(citems);
            }
        }
//        versionLabel.setText(BVersion.version);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel atOnceLabel;
    private javax.swing.JRadioButton autodetectType;
    private javax.swing.JLabel cDAVComponentTypeLabel;
    private javax.swing.JTextField cDAVConcurrent;
    private javax.swing.JTextField cDLBulk;
    private javax.swing.JRadioButton calDavType;
    private javax.swing.JTextField cdataType;
    private javax.swing.JLabel cdataType_label;
    private javax.swing.JTextField cdav_component;
    private javax.swing.JTextField cdav_end;
    private javax.swing.JTextField cdav_property;
    private javax.swing.JTextField cdav_start;
    private javax.swing.JLabel concurrentLabel;
    private javax.swing.JCheckBox convertAlarms;
    private javax.swing.JLabel default_note;
    private javax.swing.JLabel dlLabel;
    private javax.swing.JCheckBox dualtodo;
    private javax.swing.JCheckBox emailSubstitute;
    private javax.swing.JLabel formTitle;
    private javax.swing.JRadioButton gdavType;
    private javax.swing.JTextField inboundClass;
    private javax.swing.JLabel inboundClass_label;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JLabel options_label;
    private javax.swing.JButton saveButton;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JTextField serverHost;
    private javax.swing.JLabel serverHost_label;
    private javax.swing.JLabel sifeNote;
    private javax.swing.JCheckBox singleLog;
    private javax.swing.JTextField sourceId;
    private javax.swing.JLabel sourceId_label;
    private javax.swing.JTable sourcesTable;
    private javax.swing.JLabel sourcesTable_label;
    private javax.swing.ButtonGroup srvTypeButtonGroup;
    private javax.swing.JTextField storeLoc;
    private javax.swing.JLabel storeLoc_label;
    private javax.swing.JLabel vcalNote;
    private javax.swing.JLabel warning;
    private javax.swing.JCheckBox zideStore;
    // End of variables declaration//GEN-END:variables
}