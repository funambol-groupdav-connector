/*
 * OutboundFunambolContactObject.java
 *
 * Created on Apr 28 2009
 * 
 * Copyright (C) 2009 credativ GmbH
 * 
 * GroupDAV connector for Funambol v6.5
 * Heavily based on OutboundFunambolContactObject.java [Copyright (C) 2007  Mathew McBride]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.groupdav.notes;

import net.bionicmessage.funambol.datakit.NoteConverter;
import net.bionicmessage.funambol.framework.OutboundObject;
import com.funambol.common.pim.converter.BaseConverter;
import com.funambol.common.pim.converter.NoteToSIFN;
import com.funambol.common.pim.note.Note;
import com.funambol.common.pim.sif.SIFNParser;
import com.funambol.common.pim.vnote.VNoteParser;
import com.funambol.framework.engine.SyncItem;
import com.funambol.framework.engine.SyncItemImpl;
import com.funambol.framework.engine.source.SyncSource;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;
import net.bionicmessage.funambol.framework.ObjectTransformationException;
import net.bionicmessage.funambol.framework.Constants;
import net.bionicmessage.funambol.groupdav.calendar.iCalSourceObject;
import net.bionicmessage.utils.QPDecode;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;

/**
 * @author Marc Brockschmidt
 */
public final class OutboundFunambolNoteObject extends iCalSourceObject implements OutboundObject {
    /**
     * The note we are working on.
     */
    private Note noteObject;
    
    /**
     * Logging object used throughout the code.
     */
    private Logger log = null;

    /** 
     * Default constructor for OutboundFunambolNoteObject, creating an
     * object based on a pre-existing ical calendar.
     * @param note a Note to be sent to the device
     * @param l Logger object used throughout the code.
     */
    public OutboundFunambolNoteObject(final Note note, final Logger l) {
        noteObject = note;
        log = l;
    }

    @Override
    public Object getPDIDataObject() {
        return noteObject;
    }

    @Override
    public Class getPDIDataClass() {
        return com.funambol.common.pim.note.Note.class;
    }

    @Override
    public String getStringRepresentation(final String type) throws ObjectTransformationException {
        try {
            if (type.equals(Constants.TYPE_NOTE_SIFN)) {
                BaseConverter conv = new NoteToSIFN(null, BaseConverter.CHARSET_UTF8);
                return conv.convert(noteObject);
            } else if (type.endsWith(Constants.TYPE_NOTE_VNOTE)) {
                return NoteConverter.convertNote2VNote(noteObject).toString();
            } else if (type.equals(Constants.TYPE_NOTE_ICAL)) {
                return NoteConverter.convertNote2ical4j(noteObject, null).toString();
            } else if (type.equals(Constants.TYPE_NOTE_PLAIN)) {
                return NoteConverter.convertNote2Plain(noteObject);
            }
        } catch (Exception e) {
            log.throwing(this.getClass().getName(), "getStringRepresentation", e);
            throw new ObjectTransformationException("Error transforming contact to" + type, e);
        }
        throw new UnsupportedOperationException("Type: " + type + " unsupported");
    }

    @Override
    public void parseIntoObjectFromString(final String str, final String type) throws ObjectTransformationException {
        try {
            parseIntoObjectFromBytes(str.getBytes("UTF-8"), type);
        } catch (UnsupportedEncodingException e) {
            throw new ObjectTransformationException("Error outputting content in UTF-8");
        }
    }
    
    /**
     * Parse byte array into a funambol Note object.
     * 
     * @param content array of bytes representing the decoded input
     * @param type data type of the input.
     * @throws ObjectTransformationException if conversion of the input fails
     */
    private void parseIntoObjectFromBytes(final byte[] content, final String type) throws ObjectTransformationException {
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(content);
            if (type.equals(Constants.TYPE_NOTE_SIFN)) {
                SIFNParser scp = new SIFNParser(bis);
                noteObject = scp.parse();
            } else if (type.equals(Constants.TYPE_NOTE_VNOTE)) {
                new VNoteParser(bis);
                noteObject = NoteConverter.convertVNote2Note(VNoteParser.VNote());
            } else if (type.equals(Constants.TYPE_NOTE_ICAL)) {
                CalendarBuilder cbuild = new CalendarBuilder();
                Calendar cd = cbuild.build(bis);
                /* Decode Quoted Printables */
                Calendar decoded = QPDecode.decodeQP(cd);
                noteObject = NoteConverter.convertical4j2Note(decoded, null);
            }
        } catch (Exception e) {
            throw new ObjectTransformationException("Error parsing from type: " + type, e);
        }
    }

    @Override
    public String getDefaultStringOutputType() {
        return Constants.TYPE_NOTE_ICAL;
    }
    
    /**
     * Create a funambol sync item from the parsed note object.
     * 
     * @param source SyncSource for which the sync item is to be created.
     * @return a syncItem corresponding to this note object
     * @throws ObjectTransformationException if converting the note object to a string fails
     * @throws UnsupportedEncodingException if transforming the output string as UTF-8 to a byte array fails
     */
    public SyncItem generateSyncItem(final SyncSource source) throws ObjectTransformationException, UnsupportedEncodingException {
        String content = null;
        String type = source.getInfo().getPreferredType().getType();
        if (type == null) {
            content = getStringRepresentation(getDefaultStringOutputType());
        } else {
            content = getStringRepresentation(type);
        }
        String uid = noteObject.getUid().getPropertyValueAsString();
        SyncItemImpl si = new SyncItemImpl(source, uid);
        byte[] con = content.getBytes("UTF-8");
        si.setContent(con);
        return si;
    }
}