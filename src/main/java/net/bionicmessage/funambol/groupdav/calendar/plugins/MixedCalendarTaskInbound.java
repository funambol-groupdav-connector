/* GroupDAV connector for Funambol v6.5
 * Copyright (C) 2006-2008  Mathew McBride
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
package net.bionicmessage.funambol.groupdav.calendar.plugins;

import com.funambol.framework.tools.beans.BeanInitializationException;
import com.funambol.framework.tools.beans.LazyInitBean;
import java.io.Serializable;
import net.bionicmessage.funambol.groupdav.calendar.InboundFunambolCalendarObject;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;


/**
 * 
 * @author matt
 */
public class MixedCalendarTaskInbound extends InboundFunambolCalendarObject 
    implements Serializable, LazyInitBean {

    public MixedCalendarTaskInbound() {
        super();
    }

    
    public void init() throws BeanInitializationException {
        
    }

    @Override
    public String getDestinationStore() {
        Calendar ical4jcal = (Calendar)this.getPDIDataObject();
        Component firstComponent = (Component)ical4jcal.getComponents().get(0);
        if (firstComponent.getName().equals(Component.VTODO)) {
            return "tasks";
        }
        return "default";
    }

    
}
