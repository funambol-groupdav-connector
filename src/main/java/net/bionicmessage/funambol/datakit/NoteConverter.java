package net.bionicmessage.funambol.datakit;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.List;
import net.bionicmessage.funambol.framework.ObjectTransformationException;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.component.VJournal;
import net.fortuna.ical4j.model.parameter.Encoding;
import net.fortuna.ical4j.model.property.Categories;
import net.fortuna.ical4j.model.property.Clazz;
import net.fortuna.ical4j.model.property.Created;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.LastModified;
import net.fortuna.ical4j.model.property.Summary;
import net.fortuna.ical4j.model.property.Uid;
import com.funambol.common.pim.note.Note;
import com.funambol.common.pim.vnote.VNoteParser;
import com.funambol.common.pim.common.Property;
import com.funambol.common.pim.common.XTag;
import com.funambol.common.pim.model.VNote;
import com.funambol.framework.engine.source.SyncContext;

/**
 * Provides utilities to convert VNote objects to note objects.
 * @author Marc Brockschmidt
 */
public final class NoteConverter {
    /**
     * Dummy constructor to prevent instantiation of this class.
     */
    private NoteConverter() {
        throw new RuntimeException("You are not supposed to instantiate the NoteConverter class");
    }
    
    /**
     * Converts a VNote object into the funambol-internal Note object.
     * @param vNoteIn VNote object
     * @return Note object (containing the same data as the VNote object)
     * @throws ObjectTransformationException iff required values are missing/unset
     */
    public static com.funambol.common.pim.note.Note convertVNote2Note(
            final com.funambol.common.pim.model.VNote vNoteIn) throws ObjectTransformationException {
        Note fnblNote = new Note();
        //This one is actually mandatory:
        if (vNoteIn.getProperty("BODY") == null) {
            throw new ObjectTransformationException("Missing BODY property");
        } else {
            fnblNote.setTextDescription(new Property(vNoteIn.getProperty("BODY").getValue()));
        }

        if (vNoteIn.getProperty("VERSION") == null) {
            throw new ObjectTransformationException("Missing VERSION property");
        }
        
        if (vNoteIn.getProperty("SUMMARY") != null) {
            fnblNote.setSubject(new Property(vNoteIn.getProperty("SUMMARY").getValue()));
        }
        
        if (vNoteIn.getProperty("CATEGORIES") != null) {
            fnblNote.setCategories(new Property(vNoteIn.getProperty("CATEGORIES").getValue()));
        }
        
        if (vNoteIn.getProperty("DCREATED") != null) {
            fnblNote.setDate(new Property(vNoteIn.getProperty("DCREATED").getValue()));
        }
        
        if (vNoteIn.getProperty("LAST-MODIFIED") != null) {
            com.funambol.common.pim.common.XTag xtag = new XTag();
            xtag.setXTagValue("X-Last-Modified");
            xtag.getXTag().setPropertyValue(vNoteIn.getProperty("LAST-MODIFIED").getValue());
            fnblNote.addXTag(xtag);
        }
        
        if (vNoteIn.getProperty("CLASS") != null) {
            com.funambol.common.pim.common.XTag xtag = new XTag();
            xtag.setXTagValue("X-Class");
            xtag.getXTag().setPropertyValue(vNoteIn.getProperty("CLASS").getValue());
            fnblNote.addXTag(xtag);
        }
        
        if (vNoteIn.getProperty("X-IRMC-LUID") != null) {
            fnblNote.setUid(new Property(vNoteIn.getProperty("X-IRMC-LUID").getValue()));
        }
        return fnblNote;
    }
    
    /**
     * Converts a funambol-internal Note object to a VNote object.
     * @param fnblNote funambol note object
     * @return VNote object (containing the same data as the Note object)
     * @throws ObjectTransformationException iff required values are missing/unset
     */
    public static com.funambol.common.pim.model.VNote convertNote2VNote(
            final com.funambol.common.pim.note.Note fnblNote) 
    throws ObjectTransformationException {
        VNote convertedVNote = new VNote();

        com.funambol.common.pim.common.Property uid = fnblNote.getUid();
        com.funambol.common.pim.common.Property dcreated = fnblNote.getDate();
        com.funambol.common.pim.common.Property subject = fnblNote.getSubject();
        com.funambol.common.pim.common.Property description = fnblNote.getTextDescription();
        com.funambol.common.pim.common.Property categories = fnblNote.getCategories();
        com.funambol.common.pim.common.Property lastModified = null;
        com.funambol.common.pim.common.Property noteClass = null;
        List<com.funambol.common.pim.common.XTag> xTags = fnblNote.getXTags();
        if (xTags != null) {
            for (com.funambol.common.pim.common.XTag t : xTags) {
                if (t.getXTagValue().equals("X-Last-Modified")) {
                    lastModified = t.getXTag();          
                }

                if (t.getXTagValue().equals("X-Class")) {
                    noteClass = t.getXTag();          
                }
            }
        }
        
        convertedVNote.setProperty(
                new com.funambol.common.pim.model.Property(
                        "VERSION",
                        "1.1"));

        if (hasValue(uid)) {
            convertedVNote.setProperty(
                    new com.funambol.common.pim.model.Property(
                            "X-IRMC-LUID",
                            uid.getPropertyValueAsString()));
        }

        if (hasValue(dcreated)) {
            convertedVNote.setProperty(
                    new com.funambol.common.pim.model.Property(
                            "DCREATED",
                            dcreated.getPropertyValueAsString()));
        }

        if (hasValue(lastModified)) {
            convertedVNote.setProperty(
                    new com.funambol.common.pim.model.Property(
                            "LAST-MODIFIED",
                            lastModified.getPropertyValueAsString()));
        }

        if (hasValue(subject)) {
            convertedVNote.setProperty(
                    new com.funambol.common.pim.model.Property(
                            "SUMMARY",
                            subject.getPropertyValueAsString()));
        }

        if (hasValue(description)) {
            convertedVNote.setProperty(
                    new com.funambol.common.pim.model.Property(
                            "BODY",
                            fnblNote.getTextDescription().getPropertyValueAsString()));
        } else {
            throw new ObjectTransformationException("Missing DESCRIPTION of note");
        }

        if (hasValue(categories)) {
            convertedVNote.setProperty(
                    new com.funambol.common.pim.model.Property(
                            "CATEGORIES",
                            categories.getPropertyValueAsString()));
        }

        if (hasValue(noteClass)) {
            convertedVNote.setProperty(
                    new com.funambol.common.pim.model.Property(
                            "CLASS",
                            noteClass.getPropertyValueAsString()));
        }

        return convertedVNote;
    }
    
    /**
     * Converts a funambol-internal Note object into an iCal calender containing only
     * one VJournal entry (corresponding to the Note).
     * 
     * @param fnblNote funambol note object
     * @param ctx sync context, used for timezone conversions
     * @return iCal calendar object (containing a VJournal entry with the same data as the Note object)
     * @throws ObjectTransformationException iff required values are missing/unset
     */
    public static net.fortuna.ical4j.model.Calendar convertNote2ical4j(
            final com.funambol.common.pim.note.Note fnblNote,
            final SyncContext ctx) throws ObjectTransformationException {
        
        net.fortuna.ical4j.model.Calendar ical4jcal =
            new net.fortuna.ical4j.model.Calendar();
        net.fortuna.ical4j.model.component.VJournal ical4jJournal =
                new net.fortuna.ical4j.model.component.VJournal();
        ical4jcal.getComponents().add(ical4jJournal);
        
        String tz = null;
        if (ctx != null && ctx.getPrincipal().getDevice().getConvertDatePolicy() == 0) {
            tz = ctx.getPrincipal().getDevice().getTimeZone();
        }
        
        com.funambol.common.pim.common.Property uid = fnblNote.getUid();
        com.funambol.common.pim.common.Property dcreated = fnblNote.getDate();
        com.funambol.common.pim.common.Property subject = fnblNote.getSubject();
        com.funambol.common.pim.common.Property description = fnblNote.getTextDescription();
        com.funambol.common.pim.common.Property categories = fnblNote.getCategories();
        com.funambol.common.pim.common.Property lastModified = null;
        com.funambol.common.pim.common.Property noteClass = null;
        List<com.funambol.common.pim.common.XTag> xTags = fnblNote.getXTags();
        if (xTags != null) {
            for (com.funambol.common.pim.common.XTag t : xTags) {
                if (t.getXTagValue().equals("X-Last-Modified")) {
                    lastModified = t.getXTag();          
                }

                if (t.getXTagValue().equals("X-Class")) {
                    noteClass = t.getXTag();
                }
            }
        }

        net.fortuna.ical4j.model.property.ProdId pri = new net.fortuna.ical4j.model.property.ProdId("-//BionicMessage Funambol Connector//funambol2ical4jconvert//EN");
        ical4jcal.getProperties().add(pri);
        ical4jcal.getProperties().add(net.fortuna.ical4j.model.property.Version.VERSION_2_0);
        if (uid != null && uid.getPropertyValueAsString() != null) {
            net.fortuna.ical4j.model.property.Uid ical4juid = new net.fortuna.ical4j.model.property.Uid(uid.getPropertyValueAsString());
            ical4jJournal.getProperties().add(ical4juid);
        }
        
        if (hasValue(dcreated)) {
            try {
                net.fortuna.ical4j.model.property.Created created = null;
                String dcreateddate = dcreated.getPropertyValueAsString();
                if (dcreateddate.length() == com.funambol.common.pim.utility.TimeUtils.PATTERN_YYYY_MM_DD_LENGTH) {
                    created = new net.fortuna.ical4j.model.property.Created();
                    created.getParameters().add(net.fortuna.ical4j.model.parameter.Value.DATE);
                    String formatted = dcreateddate.replace("-", "");
                    created.setValue(formatted);
                } else {
                    net.fortuna.ical4j.model.DateTime dtdt = 
                        net.bionicmessage.funambol.datakit.funambol2ical4j.parseIntoDateTime(dcreateddate, tz);
                    created = new net.fortuna.ical4j.model.property.Created(dtdt);
                }
                ical4jJournal.getProperties().add(created);
            } catch (Exception ex) {
                throw new ObjectTransformationException("Error in CREATED conversion", ex);
            }
        }
        
        if (hasValue(lastModified)) {
            try {
                net.fortuna.ical4j.model.property.LastModified lastMod = null;
                String lastModDate = lastModified.getPropertyValueAsString();
                if (lastModDate.length() == com.funambol.common.pim.utility.TimeUtils.PATTERN_YYYY_MM_DD_LENGTH) {
                    lastMod = new net.fortuna.ical4j.model.property.LastModified();
                    lastMod.getParameters().add(net.fortuna.ical4j.model.parameter.Value.DATE);
                    String formatted = lastModDate.replace("-", "");
                    lastMod.setValue(formatted);
                } else if (lastModDate.length() > com.funambol.common.pim.utility.TimeUtils.PATTERN_YYYY_MM_DD_HH_MM_SS_LENGTH) {
                    //lastModDate is a funambol-internal timestamp, has ".NN" attached, for higher precision ... we throw it away:
                    lastModDate = lastModDate.substring(0, com.funambol.common.pim.utility.TimeUtils.PATTERN_YYYY_MM_DD_HH_MM_SS_LENGTH);
                    SimpleDateFormat sdf = new SimpleDateFormat(com.funambol.common.pim.utility.TimeUtils.PATTERN_YYYY_MM_DD_HH_MM_SS);
                    java.util.Date ds = sdf.parse(lastModDate);
                    net.fortuna.ical4j.model.DateTime dtdt = new net.fortuna.ical4j.model.DateTime(ds);
                    if (tz != null) {
                        dtdt.setUtc(true);
                    }
                    lastMod = new net.fortuna.ical4j.model.property.LastModified(dtdt);
                } else {
                    net.fortuna.ical4j.model.DateTime dtdt = 
                        net.bionicmessage.funambol.datakit.funambol2ical4j.parseIntoDateTime(lastModDate, tz);
                    lastMod = new net.fortuna.ical4j.model.property.LastModified(dtdt);
                }
                ical4jJournal.getProperties().add(lastMod);
            } catch (Exception ex) {
                //log.severe("Error thrown in dtstamp conversion: " + ex.getMessage());
                throw new ObjectTransformationException("Error in LAST-MODIFIED conversion", ex);
            }
        }

        if (hasValue(subject)) {
            net.fortuna.ical4j.model.property.Summary ical4jsumm =
                    new net.fortuna.ical4j.model.property.Summary(subject.getPropertyValueAsString());
            ical4jJournal.getProperties().add(ical4jsumm);
        } else {
            net.fortuna.ical4j.model.property.Summary blankSumm =
                    new net.fortuna.ical4j.model.property.Summary("No summary entered");
            ical4jJournal.getProperties().add(blankSumm);
        }

        if (hasValue(description)) {
            net.fortuna.ical4j.model.property.Description ical4jdescription =
                    new net.fortuna.ical4j.model.property.Description(description.getPropertyValueAsString());
            ical4jJournal.getProperties().add(ical4jdescription);
        }

        if (hasValue(categories)) {
            net.fortuna.ical4j.model.property.Categories cat =
                    new net.fortuna.ical4j.model.property.Categories();
            cat.setValue(categories.getPropertyValueAsString());
            ical4jJournal.getProperties().add(cat);
        }
        
        if (hasValue(noteClass)) {
            net.fortuna.ical4j.model.property.Clazz noteClazz =
                    new net.fortuna.ical4j.model.property.Clazz();
            noteClazz.setValue(noteClass.getPropertyValueAsString());
            ical4jJournal.getProperties().add(noteClazz);
        }

        return ical4jcal;
    }
    
    /**
     * Converts an iCal calender containing only one VJournal entry into a
     * funambol-internal Note object.
     * 
     * @param ical4jcal iCal calendar object 
     * @param ctx sync context, used for timezone conversions
     * @return funambol note object
     * @throws ObjectTransformationException iff required values are missing/unset
     */
    public static com.funambol.common.pim.note.Note convertical4j2Note(
            final net.fortuna.ical4j.model.Calendar ical4jcal,
            final SyncContext ctx) throws ObjectTransformationException {
        com.funambol.common.pim.note.Note fnblNote = new Note();
        
        net.fortuna.ical4j.model.component.VJournal ical4jNote = 
            (VJournal) ical4jcal.getComponents().getComponent(Component.VJOURNAL);
        
        if (ical4jNote.getProperty(Uid.UID) != null) {
            Uid uid = (Uid) ical4jNote.getProperty(Uid.UID);
            com.funambol.common.pim.common.Property uidprop =
                    new com.funambol.common.pim.common.Property(uid.getValue());
            fnblNote.setUid(uidprop);
        }
        
        if (ical4jNote.getProperty(Created.CREATED) != null) {
            Created created = (Created) ical4jNote.getProperty(Created.CREATED);
            created.setUtc(true);
            com.funambol.common.pim.common.Property createdProp = new com.funambol.common.pim.common.Property(created.getValue());
            fnblNote.setDate(createdProp);
        }
        
        if (ical4jNote.getProperty(LastModified.LAST_MODIFIED) != null) {
            LastModified lastMod = (LastModified) ical4jNote.getProperty(LastModified.LAST_MODIFIED);
            lastMod.setUtc(true);
            com.funambol.common.pim.common.XTag xtag = new XTag();
            xtag.setXTagValue("X-Last-Modified");
            xtag.getXTag().setPropertyValue(lastMod.getValue());
            fnblNote.addXTag(xtag);
        }
        
        if (ical4jNote.getProperty(Summary.SUMMARY) != null) {
            Summary summ = (Summary) ical4jNote.getProperty(Summary.SUMMARY);
            com.funambol.common.pim.common.Property summprop =
                    new com.funambol.common.pim.common.Property();
            // Check for encodings
            if (summ.getParameter(Encoding.ENCODING) != null) {
                Encoding enc = (Encoding) summ.getParameter(Encoding.ENCODING);
                summprop.setEncoding(enc.getValue());
            }
            summprop.setPropertyValue(summ.getValue());
            fnblNote.setSubject(summprop);
        }
        
        if (ical4jNote.getProperty(Description.DESCRIPTION) != null) {
            /* We also need to consider alternative text encoding here */
            Description des = (Description) ical4jNote.getProperty(Description.DESCRIPTION);
            com.funambol.common.pim.common.Property desprop =
                    new com.funambol.common.pim.common.Property();
            if (des.getParameter(Encoding.ENCODING) != null) {
                Encoding enc = (Encoding) des.getParameter(Encoding.ENCODING);
                desprop.setEncoding(enc.getValue());
            }
            desprop.setPropertyValue(des.getValue());
            fnblNote.setTextDescription(desprop);
        }
        
        if (ical4jNote.getProperty(Categories.CATEGORIES) != null) {
            Categories cat = (Categories) ical4jNote.getProperty(Categories.CATEGORIES);
            fnblNote.getCategories().setPropertyValue(cat.getValue());
        }
        
        if (ical4jNote.getProperty(Clazz.CLASS) != null) {
            Clazz noteClass = (Clazz) ical4jNote.getProperty(Clazz.CLASS);
            com.funambol.common.pim.common.XTag xtag = new XTag();
            xtag.setXTagValue("X-Class");
            xtag.getXTag().setPropertyValue(noteClass.getValue());
            fnblNote.addXTag(xtag);
        }
        
        return fnblNote;
    }
    
    /**
     * Converts a plain text note into a funambol-internal Note object.
     * 
     * @param note String representation of the actual note
     * @param timestamp funambol sync timestamp
     * @return funambol note object
     * @throws ObjectTransformationException iff required values are missing/unset
     */
    public static com.funambol.common.pim.note.Note convertPlain2Note(
            final String note,
            final String timestamp) throws ObjectTransformationException {
        com.funambol.common.pim.note.Note fnblNote = new Note();
        fnblNote.setSubject(new Property(note));
        com.funambol.common.pim.common.XTag xtag = new XTag();
        xtag.setXTagValue("X-Last-Modified");
        xtag.getXTag().setPropertyValue(timestamp);
        fnblNote.addXTag(xtag);
        return fnblNote;
    }
    
    /**
     * Converts a funambol-internal Note object into a plain text note.
     * 
     * @param fnblNote funambol note object
     * @return note String representation of the actual note
     * @throws ObjectTransformationException iff required values are missing/unset
     */
    public static String convertNote2Plain(
            final com.funambol.common.pim.note.Note fnblNote) throws ObjectTransformationException {
        String textPlainVersion = fnblNote.getSubject().getPropertyValueAsString();
        if (textPlainVersion != null) {
            if (fnblNote.getTextDescription().getPropertyValueAsString() != null) {
                textPlainVersion += " : " + fnblNote.getTextDescription().getPropertyValueAsString();
            }
        } else {
            if (fnblNote.getTextDescription().getPropertyValueAsString() != null) {
                textPlainVersion = fnblNote.getTextDescription().getPropertyValueAsString();
            }
        }
        if (textPlainVersion == null) {
            textPlainVersion = "";
        }
        return textPlainVersion;
    }
    
    /**
     * Stub main method for easier testing.
     * @param argv Command-line arguments. Not used.
     */
    public static void main(final String[] argv) {
        String contentString = "BEGIN:VNOTE\nVERSION:1.1\nSUMMARY:Test\nBODY:This sucks!\nDCREATED:20010601T100000Z\nLAST-MODIFIED:20010602T100000Z\nCLASS:PUBLIC\nEND:VNOTE\n";
        ByteArrayInputStream bis = new ByteArrayInputStream(contentString.getBytes());
        new VNoteParser(bis);
        try {
            Note n = convertVNote2Note(VNoteParser.VNote());
            System.out.println(contentString);
            System.out.println(convertNote2VNote(n).toString());
            System.out.println(convertNote2ical4j(n, null).toString());
            System.out.println(convertNote2ical4j(convertical4j2Note(convertNote2ical4j(n, null), null), null).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Convenience method checking if a property is non-null and non-empty.
     * @param prop Property to check
     * @return true iff the property is set to an actual value
     */
    private static boolean hasValue(final com.funambol.common.pim.common.Property prop) {
        return (prop != null && prop.getPropertyValueAsString() != null 
                && prop.getPropertyValueAsString().length() > 0);
    }
}
