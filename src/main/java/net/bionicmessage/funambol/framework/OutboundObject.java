package net.bionicmessage.funambol.framework;


/**
 *  <ul><b><i>OutboundObject</i></b><i> </i>represents a <i>SourceObject</i> 
 *    enroute to the device via Funambol<i> </i>
 *      </ul>
 */
// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.3E3EAAFC-86BD-76FD-7784-7BEB4DAA63E3]
// </editor-fold> 
public interface OutboundObject extends SourceObject {

}

